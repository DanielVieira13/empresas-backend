exports.up = function (knex) {
    return knex.schema.createTable('enterprises', table => {
        table.increments('id').primary()
        table.string('name').notNull()
        table.integer('type')
        table.datetime('deletedAt').defaultTo(null)
    })
};

exports.down = function (knex) {
    return knex.schema.dropTable('enterprises')
};
