module.exports = app => {
    const save = (req, res) => {
        const enterprise = {
            ...req.body
        }
        app.db('enterprises')
            .insert(enterprise)
            .then(_ => res.status(204).send())
            .catch(err => res.status(500).send(err))
    }
    const get = (req, res) => {
        app.db('enterprises')
            .select('id', 'name', 'type')
            .whereNull('deletedAt')
            .then(enterprises => res.json(enterprises))
            .catch(err => res.status(500).send(err))
    }

    const getById = (req, res) => {
        app.db('enterprises')
            .select('id', 'name', 'type')
            .where({ id: req.params.id })
            .whereNull('deletedAt')
            .first()
            .then(enterprise => res.json(enterprise))
            .catch(err => res.status(500).send(err))
    }

    const getByType = (req, res) => {
        app.db('enterprises')
            .select('id', 'name', 'type')
            .where({
                enterprise_types: req.params.enterprise_types
            })
            .whereNull('deletedAt')
            .first()
            .then(enterprise => res.json(enterprise))
            .catch(err => res.status(500).send(err))
    }

    const getByNameAndType = (req, res) => {
        app.db('enterprises')
            .select('id', 'name', 'type')
            .where({
                type: req.params.enterprise_types,
                name: req.params.name
            })
            .whereNull('deletedAt')
            .first()
            .then(enterprise => res.json(enterprise))
            .catch(err => res.status(500).send(err))
    }

    return {
        save,
        get,
        getById,
        getByNameAndType,
        getByType
    }
}