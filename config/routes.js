module.exports = app => {
    app.all('/', (req, res) => {
        res.status(200).send('Empresas Backend')
    })

    app.all('/api', (req, res) => {
        res.status(200).send('Empresas API')
    })
}