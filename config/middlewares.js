const bodyParser = require('body-parser')
const cors = require('cors')
const version = require('./version')

module.exports = app => {
    app.use(bodyParser.json())
    app.use(cors())
    app.use(version)
}