const admin = require('./admin')

module.exports = app => {
    // V1 routes   
    app.all('/api/v1', (req, res) => {
        res.status(200).send('Empresas API Rotas V1')
    })
    // Start Auth
    app.post('/api/v1/users/auth/signup', app.api.user.save)
    app.post('/api/v1/users/auth/sign_in', app.api.auth.signin)
    app.post('/api/v1/users/auth/validateToken', app.api.auth.validateToken)
    // End Auth

    // Start User 
    app.route('/api/v1/users')
        .all(app.config.passport.authenticate())
        .post(admin(app.api.user.save))
        .get(admin(app.api.user.get))

    app.route('/api/v1/users/:id')
        .all(app.config.passport.authenticate())
        .put(admin(app.api.user.save))
        .get(admin(app.api.user.getById))
        .delete(admin(app.api.user.remove))
    // End User

    // Enterprises
    app.route('/api/v1/enterprises')
        .all(app.config.passport.authenticate())
        .post(app.api.enterprise.save)
        .get(app.api.enterprise.get)

    app.route('/api/v1/enterprises/:id')
        .all(app.config.passport.authenticate())
        .put(admin(app.api.enterprise.save))
        .get(admin(app.api.enterprise.getById))
        .delete(admin(app.api.enterprise.remove))

    app.route('/api/api_version/enterprises/:enterprise_types/:name')
        .all(app.config.passport.authenticate())
        // Filtro de Empresas por nome e tipo
        .get(admin(app.api.enterprise.getByNameAndType))

    app.route('/api/v1/enterprises/type/:enterprise_types')
        .all(app.config.passport.authenticate())
        // Filtro de Empresas por tipo
        .get(admin(app.api.enterprise.getByType))
    // End Enterprises
    //End V1 routes
}