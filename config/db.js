const config = require('../knexfile.js')
const knex = require('knex')(config)

// roda um rollback para limpar o banco e tirar a sujeira dos testes
// knex.migrate.rollback([config])
const sow = () => {
    return new Promise(resolve => {
        knex.seed.run([config])
        resolve()
    })
}

// roda todas as migrations do sistema
knex.migrate.latest([config])
    // roda todas as seeders do sistema
    .then(sow)

module.exports = knex

