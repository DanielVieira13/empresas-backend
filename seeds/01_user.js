const bcrypt = require('bcrypt-nodejs')
const salt = bcrypt.genSaltSync(10)

exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('users').del()
    .then(function () {
        // Inserts seed entries
        return knex('users').insert([
            {
            id: 1,
            name: 'Teste Apple',
            email: 'testeapple@ioasys.com.br',
            password: bcrypt.hashSync('12341234', salt),
            admin: true,
            deletedAt: null
            },
            {
            id: 2,
            name: 'Lucas Rizel',
            email: 'lucasrizel@ioasys.com.br',
            password: bcrypt.hashSync('12345678', salt),
            admin: true,
            deletedAt: null
            }
        ]);
    });
};
