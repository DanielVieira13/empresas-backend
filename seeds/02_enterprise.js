exports.seed = function (knex) {
    // Deletes ALL existing entries
    return knex('enterprises').del()
        .then(function () {
            // Inserts seed entries
            return knex('enterprises').insert([
                {
                    id: 1,
                    name: 'aQm',
                    type: '1'
                },
                {
                    id: 2,
                    name: 'enterprise 2',
                    type: '2'
                },
                {
                    id: 3,
                    name: 'enterprise 3',
                    type: '1'
                },
                {
                    id: 4,
                    name: 'enterprise 4',
                    type: '1'
                },
                {
                    id: 5,
                    name: 'enterprise 5',
                    type: '2'
                }
            ]);
        });
};