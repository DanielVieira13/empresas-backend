const app = require('express')()
const consign = require('consign')
const db = require('./config/db')

app.db = db

consign()
    .include('./config/passport.js')
    .then('./config/routes.js')
    .then('./config/middlewares.js')
    .then('./api/validation.js')
    .then('./api')
    .then('./config/routes_v1.js')
    // .then('./config/routes_v2.js')
    .into(app)

app.listen(3000, () => {
    console.log('Backend executando...')
})